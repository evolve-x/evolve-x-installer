# Evolve-X-Installer

A small little installer program to install Evolve-X and validate everything, designed to hopefully work on all UNIX systems and uses no pakcage managers besides NPM.

# Makes installing Evolve-X and determining uninstalled pre reqeusites easier

Install with git via `git clone https://gitlab.com/evolve-x/evolve-x-installer`

Set file to executable
- cd {directory installed in}
- chmod +x installer.sh

and just run...

`./installer.sh`

This will tell you if some requirement is not installed and install the server, its dependencies, and build it for you.