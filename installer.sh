#! /bin/sh


if ! [ $(id -u) = 0 ] && ! [ $(command -v tsc) ]; then
    echo "I need root to run this command"
    exit 1
fi

echo "[PRE REQEUSITES] Checking Node..."
if ! [ $(command -v node) ]; then
    echo "[FATAL] Node not found!\nInstall NodeJS, https://nodejs.org"
    exit 1
fi
echo "[PRE REQEUSITES] Node found"

install_evolvex () {
    echo "[PRE REQEUSITES] Checking mongo..."
    mongo=$(command -v mongo)
    if ! [ $mongo ]; then
        echo "[FATAL] MongoDB not found!\nInstall MongoDB, https://mongodb.org"
        exit 1
    fi
    echo "[PRE REQEUSITES] MongoDB found\n"
    echo "Where would you like to install Evolve-X?"
    read DIR
    wait
    if ! [ -d $DIR ]; then
        echo "Not a directory"
        exit 1
    fi
    cd $DIR
    cd ../
    echo "[INSTALL 1/3] Installing Evolve-X"
    git clone https://gitlab.com/evolve-x/evolve-x $DIR
    wait

    cd $DIR
    echo "[INSTALL 2/3] Installing dependencies"
    npm i
    wait
    if [ $(id -u) = 0 ] && ! [ $(command -v tsc) ]; then
        echo "[INSTALL 2.5/3] Installing requirement TypeScript...."
        npm i -g typescript
        wait
    fi
    echo "[INSTALL 3/3] Building source..."
    tsc
    wait
    echo "[SUCCESS] Evolve-X installed"
    exit 0
}

install_efvs () {
  echo "[PRE REQEUSITES] Checking mongo..."
    mongo=$(command -v mongo)
    if ! [ $mongo ]; then
        echo "[FATAL] MongoDB not found!\nInstall MongoDB, https://mongodb.org"
        exit 1
    fi
    echo "[PRE REQEUSITES] MongoDB found!\n"
    echo "Where would you like to install Evolve-X FS-Validator?"
    read DIR
    wait
    if ! [ -d $DIR ]; then
        echo "Not a directory"
        exit 1
    fi
    cd $DIR
    cd ../
    echo "[INSTALL 1/3] Installing Evolve-X FS-Validator"
    git clone https://gitlab.com/evolve-x/fs-validator $DIR
    wait

    cd $DIR
    echo "[INSTALL 2/3] Installing dependencies"
    npm i
    wait
    if [ $(id -u) = 0 ] && ! [ $(command -v tsc) ]; then
        echo "[INSTALL 2.5/3] Installing requirement TypeScript...."
        npm i -g typescript
        wait
    fi
    echo "[INSTALL 3/3] Building source..."
    tsc
    wait
    echo "[SUCCESS] Evolve-X FS-Validator installed!"
    exit 0
}

install_excli () {
    echo "Where would you like to install Evolve-X CLI?"
    read DIR
    wait
    if ! [ -d $DIR ]; then
        echo "Not a directory"
        exit 1
    fi
    cd $DIR
    cd ../
    echo "[INSTALL 1/3] Installing Evolve-X CLI"
    git clone https://gitlab.com/evolve-x/ex-cli $DIR
    wait

    cd $DIR
    echo "[INSTALL 2/3] Installing dependencies"
    npm i
    wait
    if [ $(id -u) = 0 ] && ! [ $(command -v tsc) ]; then
        echo "[INSTALL 2.5/3] Installing requirement TypeScript...."
        npm i -g typescript
        wait
    fi
    echo "[INSTALL 3/3] Building source..."
    tsc
    wait
    echo "[SUCCESS] Evolve-X CLI installed!"
    exit 0
}

echo "Would you like to install Evolve-X (ex), Evolve-X FS-Validator (efsv) or the Evolve-X CLI (excli)?"
read CHOICE

if [ $CHOICE = "ex" ]; then
    install_evolvex
    wait

elif [ $CHOICE = "efsv" ]; then
    install_efvs
    wait
elif [ $CHOICE = "excli" ]; then
    install_excli
    wait
fi

echo "Not an option. Goodbye!"
exit 1

